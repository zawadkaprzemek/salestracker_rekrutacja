<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture
{
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user=new User();
        $user
            ->setFirstName('Przemysław')
            ->setLastName('Zawadka')
            ->setBirthDate( (new \DateTime('1989-06-14')))
            ->setEmail("zawadkaprzemek@gmail.com")
            ->setPassword($this->encoder->hashPassword($user,"HasłoMasło12345"))
        ;

        $salesUser=new User();
        $salesUser
            ->setFirstName('Sales')
            ->setLastName('Tracker')
            ->setBirthDate( (new \DateTime('2014-05-12')))
            ->setEmail("info@salestracker24.com")
            ->setPassword($this->encoder->hashPassword($user,"SalesTracker2021"))
        ;
        $manager->persist($user);
        $manager->persist($salesUser);


        $manager->flush();
    }
}
