<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class MailerService
{
    const sender = 'kontakt@gpvoting.pl';
    private Environment $twig;
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mail,Environment $twig)
    {
        $this->mailer = $mail;
        $this->twig = $twig;
    }

    public function sendEmailWithPassword($email, $password)
    {
        $body=$this->renderView(
                'email/new_account_password_email.html.twig',['password'=>$password]
            );
        $title='Hasło do konta SalesTracker Rekrutacja';
        $this->sendMail($email,$title,$body);
    }

    private function renderView(string $template,array $params=[]): string
    {
        return $this->twig->render($template,$params);
    }

    private function sendMail(string $recipent,string $title,string $body)
    {
        $mail = (new Email())
            ->from(self::sender)
            ->priority(Email::PRIORITY_HIGH)
            ->subject($title)
            ->addTo($recipent)
            ->html($body);

        $this->mailer->send($mail);
    }
}