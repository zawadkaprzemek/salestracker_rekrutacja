<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ImportExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('importError', [$this, 'importError']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('importError', [$this, 'importError']),
        ];
    }

    public function importError($value): string
    {
        $text='';
        switch ($value){
            case 'Invalid_data':
                $text="Złe dane";
                break;
            case 'User_exists':
                $text="Użytkownik istnieje z takim mailem";
                break;
            default:
                break;
        }
        return $text;
    }
}
