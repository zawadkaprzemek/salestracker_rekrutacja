<?php

namespace App\Traits;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

trait ParameterBagTrait
{

    private ParameterBagInterface $parameterBag;

    private function getParameter(string $name)
    {
        return $this->parameterBag->get($name);
    }
}