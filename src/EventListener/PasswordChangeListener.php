<?php

namespace App\EventListener;


use App\Entity\User;
use App\Traits\ParameterBagTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PasswordChangeListener
{
    use ParameterBagTrait;
    private TokenStorageInterface $security;

    private RouterInterface $router;

    const CHANGE_PASSWORD_ROUTE= 'app_change_password';
    const IGNORED_ROUTES = [
        'app_change_password',
        'app_login',
        'app_logout',
    ];

    public function __construct(
        TokenStorageInterface $security,
        RouterInterface $router,
        ParameterBagInterface $parameterBag
    ) {
        $this->security = $security;
        $this->router = $router;
        $this->parameterBag = $parameterBag;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (false === $event->isMainRequest()) {
            return;
        }

        if ($event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $currentRoute = $event->getRequest()->get('_route');
        if (in_array($currentRoute, self::IGNORED_ROUTES, true)) {
            return;
        }

        $token = $this->security->getToken();

        if (null === $token) {
            return;
        }

        /**
         * @var User $user
         */
        $user = $token->getUser();

        if ($user instanceof UserInterface && $this->forceChangePassword($user)) {
            $response = new RedirectResponse($this->router->generate(self::CHANGE_PASSWORD_ROUTE));
            $event->setResponse($response);
        }
    }

    private function forceChangePassword(User $user): bool
    {
        $lastChange= $user->getLastPasswordChange() ?? $user->getCreatedAt();
        $now= new \DateTime();
        $diff=$now->diff($lastChange);
        return $diff->days>=(int)$this->getParameter('days_to_change_password');
    }
}