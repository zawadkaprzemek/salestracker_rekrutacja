<?php

namespace App\Message;

final class NewAccountMessage
{
    /*
     * Add whatever properties & methods you need to hold the
     * data for this message class.
     */

    private string $email;

    private string $password;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
