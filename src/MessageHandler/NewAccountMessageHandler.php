<?php

namespace App\MessageHandler;

use App\Message\NewAccountMessage;
use App\Service\MailerService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class NewAccountMessageHandler implements MessageHandlerInterface
{
    private MailerService $mailerService;

    public function __construct(MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
    }

    public function __invoke(NewAccountMessage $message)
    {
        $this->mailerService->sendEmailWithPassword($message->getEmail(),$message->getPassword());
    }
}
