<?php

namespace App\Controller;


use App\Entity\User;
use App\Form\PasswordChangeType;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/profile")
 * @IsGranted("ROLE_USER")
 */
class ProfileController extends AbstractController
{

    private TranslatorInterface $translator;
    private ManagerRegistry $managerRegistry;

    public function __construct(TranslatorInterface $translator,ManagerRegistry $managerRegistry)
    {
        $this->translator = $translator;
        $this->managerRegistry = $managerRegistry;
    }
    /**
     * @Route("/", name="app_profile")
     */
    public function index(): Response
    {
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }

    /**
     * @Route("/change_password", name="app_change_password")
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function changePassword(Request $request,UserPasswordHasherInterface $passwordEncoder)
    {
        /** @var User $user */
        $user=$this->getUser();
        $form=$this->createForm(PasswordChangeType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            if(!$passwordEncoder->isPasswordValid($user,$form->get('oldPassword')->getData())){
                $form->get('oldPassword')->addError(new FormError($this->translator->trans('form.error.wrong_password')));
            }else{
                if($form->get('oldPassword')->getData()===$form->get('plainPassword')->getData())
                {
                    $form->get('plainPassword')->get('first')->addError(new FormError($this->translator->trans('form.error.change_password')));
                }
            }
            if($form->isValid())
            {
                $newpassword=$passwordEncoder->hashPassword($user, $form->get('plainPassword')->getData());
                $user->updatePassword($newpassword);
                $entityManager = $this->managerRegistry->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', $this->translator->trans('profile.change_password.success'));
                return $this->redirectToRoute('app_profile');
            }
        }

        return $this->render('profile/change_password.html.twig',[
            'form'=>$form->createView()
        ]);
    }
}
