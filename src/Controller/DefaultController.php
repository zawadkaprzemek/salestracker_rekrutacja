<?php

namespace App\Controller;

use App\Entity\User;
use App\Message\NewAccountMessage;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use SimpleXLS;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ImportUsersType;

/**
 * @IsGranted("ROLE_USER")
 */
class DefaultController extends AbstractController
{

    private ManagerRegistry $managerRegistry;
    private MessageBusInterface $messageBus;

    public function __construct(ManagerRegistry $managerRegistry,MessageBusInterface $messageBus)
    {
        $this->managerRegistry = $managerRegistry;
        $this->messageBus = $messageBus;
    }
    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }


    /**
     * @Route("/users/{page}", name="app_users_list", requirements={"page":"\d+"}, defaults={"page":1})
     * @param UserRepository $repository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function usersList(UserRepository $repository,PaginatorInterface $paginator,int $page): Response
    {
        /** @var User $user */
        $user=$this->getUser();
        return $this->render('default/users.html.twig', [
            'pagination' => $paginator->paginate(
                $repository->findAllOthers($user),$page,$this->getParameter('items_per_page'),[]),
            'manage'=>false
        ]);
    }

    /**
     * @Route("/import_users", name="app_import_users")
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordEncoder
     * @return Response
     */
    public function importUsersAction(Request $request,UserPasswordHasherInterface $passwordEncoder): Response
    {
        $form=$this->createForm(ImportUsersType::class);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $file=$form->getData()['file'];
            try{
                $stats=$this->parseFile($file,$passwordEncoder);
            }catch(\Throwable $ex)
            {
                $error = $ex->getMessage();
            }

        }

        return $this->render('default/import.html.twig',[
            'form' => $form->createView(),
            'stats'=> $stats ?? null,
            'error'=> $error ?? null
        ]);
    }

    private function parseFile($file,UserPasswordHasherInterface $passwordEncoder): array
    {
        $xls=SimpleXLS::parse($file);
        $em=$this->managerRegistry->getManager();
        $repository=$this->managerRegistry->getRepository('App:User');
        $stats=array('new'=>array(),'error'=>array());
        foreach ($xls->rows() as $num=> $row)
        {
            list($valid,$error)=$this->validRow($row,$repository);
            if($valid)
            {
                $user=new User();
                $user
                    ->setFirstName($row[0])
                    ->setLastName($row[1])
                    ->setEmail($row[2])
                    ->setBirthDate(new \DateTime($row[3]))
                ;
                $password=$this->generatePassword();
                $user->setPassword($passwordEncoder->hashPassword($user,$password));
                $em->persist($user);
                $this->messageBus->dispatch(
                    new NewAccountMessage(
                        $user->getEmail(),
                        $password
                    )
                );
                $stats['new'][]=$user;
            }else{
                $stats['error'][$num+1]=$error;
            }
            $em->flush();

        }
        return $stats;
    }

    /**
     * @param $row
     * @param UserRepository $repository
     * @return array
     */
    private function validRow($row, UserRepository $repository): array
    {
        $valid=true;
        $error=null;
        if(
            gettype($row[0])=='string'&&
            gettype($row[1])=='string'&&
            filter_var($row[2], FILTER_VALIDATE_EMAIL)&&
            $this->checkDate($row[3])
        ){
            $exist=$repository->findOneBy(['email'=>$row[2]]);
            if(!is_null($exist))
            {
                $valid=false;
                $error='User_exists';
            }
        }else{
            $valid=false;
            $error='Invalid_data';
        }
        return array(
            $valid,
            $error
        );
    }

    private function checkDate($date): bool
    {
        try {
            $date = new \DateTime($date);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function generatePassword(): string
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789_-";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
