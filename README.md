# salesTracker24_rekrutacja

Instalacja
1. ściągnij repozytorium
2. composer install
3. w pliku .env w sekcji doctrine/doctrine-bundle wprowadź swoje dane aby połączyć się z bazą danych
4. w pliku .env w sekcji symfony/mailer wprowadź dane dostępu do skrzynki pocztowej żeby można było za jej pomocą wysyłać maile z hasłem
5. w pliku /src/Service/MailerService ustaw stałą const sender na adres podpiętej skrzynki
6. symfony server:start
7. bin/console doctrine:database:create
8. bin/console doctrine:migrations:migrate
9. bin/console doctrine:fixtures:load aby dodać do bazy konta użytkowników
10. bin/console messenger:consume do obsługi kolejki wysyłania maili z dostepami po imporcie użytkowników

Używanie:
Adres http://localhost:8000
Na początku dostępne są dwa konta użytkowników:
1. email: info@salestracker24.com z hasłem: SalesTracker2021
2. email: zawadkaprzemek@gmail.com z hasłem: HasłoMasło12345

Po zalogowaniu na dowolne z tych kont mamy dostęp do listy pozostałych użytkowników, 
możliwość importowania nowych użytkowników z pliku.
W services.yaml ustawiony jest parametr 'days_to_change_password' odpowiedzialny za wymuszanie zmiany hasła po ilości dni od ostatniej zmiany
